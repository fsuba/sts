# sts - Storage Tests

[![PyPI version](https://badge.fury.io/py/sts-libs.svg)](https://badge.fury.io/py/sts-libs)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![Documentation](https://img.shields.io/badge/docs-latest-brightgreen.svg)](https://rh-kernel-stqe.gitlab.io/sts)

Storage testing framework for Fedora, CentOS Stream, and RHEL.

## Documentation

Full documentation is available at [rh-kernel-stqe.gitlab.io/sts](https://rh-kernel-stqe.gitlab.io/sts)
