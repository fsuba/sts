# Loop Devices

This section documents the loop device functionality, which provides interfaces for working with loop devices.
Loop devices allow regular files to be accessed as block devices.

::: sts.loop
