# Base Classes

This section documents the base classes that provide
core functionality for all storage devices.

## Device Base Classes

::: sts.base
