# Device Mapper

This section documents the Device Mapper functionality, which provides
interfaces for working with Linux's device-mapper framework.

## Device Mapper Core

::: sts.dm

## Device Mapper Persistent Data

::: sts.dmpd
