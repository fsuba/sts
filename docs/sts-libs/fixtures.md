# Test Fixtures

This section documents the test fixtures provided by sts-libs.
These fixtures help set up test environments for various storage technologies.

## Common Fixtures

::: sts.fixtures.common_fixtures

## iSCSI Fixtures

::: sts.fixtures.iscsi_fixtures

## LVM Fixtures

::: sts.fixtures.lvm_fixtures

## RDMA Fixtures

::: sts.fixtures.rdma_fixtures

## Stratis Fixtures

::: sts.fixtures.stratis_fixtures

## Target Fixtures

::: sts.fixtures.target_fixtures
