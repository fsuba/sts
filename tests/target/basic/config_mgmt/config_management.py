"""Test suite of targetcli saveconfig, clearconfig and restoreconfig."""

from __future__ import annotations

import gzip
from os import getenv
from pathlib import Path
from tempfile import NamedTemporaryFile

import pytest

from sts import get_sts_host
from sts.target import Targetcli
from sts.utils.cmdline import run
from sts.utils.files import count_files

# Parameters
target = Targetcli('/iscsi/')  # Base path for iSCSI operations
backups: int = int(getenv('BACKUP_NUMBER', '5'))
max_backup_files: int = int(getenv('MAX_BACKUP_FILES', '2'))

BACKUP_DIR = '/etc/target/backup/'


@pytest.fixture(autouse=True)
def _cleanup() -> None:
    """Clean up target configuration before each test."""
    run('targetcli clearconfig confirm=true')


@pytest.mark.usefixtures('_target_test')
class TestSaveconfig:
    def test_saveconfig_cleanup(self) -> None:
        """Removes backups and saveconfig.json."""
        assert run('rm -f /etc/target/backup/*').succeeded
        assert run('rm -f /etc/target/saveconfig*.json').succeeded

    def test_saveconfig_backup(self) -> None:
        """There's only 1 backup file generated if no target change, no matter how many times running `targetcli
        saveconfig`.
        """
        # require backups >= 2, because backup file is generated at the second time of running saveconfig after a target
        # change
        assert backups >= 2
        for _ in range(backups):
            assert run('targetcli saveconfig').succeeded

        number = count_files(BACKUP_DIR)
        assert number == 1, f'FAIL: In {BACKUP_DIR} there is more than 1 backup file.'

    def test_saveconfig_compare(self) -> None:
        """Compares last saved configuration with backup file."""
        # Create iscsi targets
        for _ in range(3):
            assert target.create().succeeded

        # Save the configuration
        for _ in range(2):
            assert run('targetcli saveconfig').succeeded

        # Compare the content of the last saved /etc/target/saveconfig.json and /etc/target/backup/backup_file
        saveconfig = Path('/etc/target/saveconfig.json').read_text().strip()

        backup_files = Path('/etc/target/backup/').glob('saveconfig-*')
        backup_file = max(backup_files, key=lambda item: item.stat().st_ctime)
        with gzip.open(backup_file, 'rt') as file:
            backup = file.read().strip()

        assert saveconfig == backup, 'FAIL: Backup file is different than last saved configuration'

    def test_max_backup_files(self) -> None:
        """Creates maximum amount of backup files."""
        assert target.set_('global', max_backup_files=str(max_backup_files)).succeeded
        assert target.get('global max_backup_files').stdout.strip() == f'max_backup_files={max_backup_files}'

        for i in range(max_backup_files + 3):
            assert target.create(wwn=f'iqn.2024-01.com.sts:target{i}').succeeded
            assert run('targetcli saveconfig').succeeded
            assert run('targetcli saveconfig').succeeded

        number = count_files(BACKUP_DIR)
        assert number <= max_backup_files

    def test_saveconfig_savefile(self) -> None:
        """Saves configuration to a specified file."""
        with NamedTemporaryFile(prefix='saveconfig-') as temp_savefile:
            savefile: str = temp_savefile.name
            assert run(f'targetcli saveconfig {savefile}').succeeded
            assert Path(savefile).exists()

    def test_restoreconfig(self) -> None:
        """Restores configuration."""
        # Backup the current saveconfig
        current_config = Path('/etc/target/saveconfig.json')
        pre_config = current_config.with_stem('saveconfig_backup')
        pre_config.write_bytes(current_config.read_bytes())

        # Check targetcli version for correct parameter
        host = get_sts_host()
        targetcli = host.package('targetcli')  # type: ignore[reportUnknownMemberType]
        clear_param = (
            'clear=true'
            if targetcli.is_installed  # type: ignore[attr-defined]
            and targetcli.version < '2.1.54'  # type: ignore[attr-defined]
            else 'clear_existing=true'
        )

        for _ in range(2):
            assert run('targetcli clearconfig confirm=true').succeeded
            assert run(f'targetcli restoreconfig {clear_param}').succeeded
            assert run('targetcli saveconfig').succeeded

        current = current_config.read_text().strip()
        pre = pre_config.read_text().strip()

        assert current == pre, 'FAIL: Current configuration is different than the base configuration'
