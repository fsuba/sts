"""Test suite for CHAP authentication."""

from __future__ import annotations

from sts.iscsi.config import set_initiatorname
from sts.iscsi.iscsiadm import IscsiAdm
from sts.target import ACL, TPG, Iscsi

# Test parameters
TARGET_IQN = 'iqn.2024-01.com.sts:target'
INITIATOR_IQN = 'iqn.2024-01.com.sts:initiator'
USERNAME = 'testuser'
PASSWORD = 'testpass'
MUTUAL_USERNAME = 'mutual_testuser'
MUTUAL_PASSWORD = 'mutual_testpass'


def test_chap_auth() -> None:
    """Test CHAP authentication.

    This test:
    1. Creates target with CHAP auth
    2. Sets up initiator with CHAP credentials
    3. Verifies login works
    4. Tests mutual CHAP auth
    """
    # Create target
    target = Iscsi(target_wwn=TARGET_IQN)
    target.create_target()

    # Set up one-way CHAP
    tpg = TPG(target_wwn=TARGET_IQN)
    tpg.disable_generate_node_acls()  # Use per-ACL auth
    acl = ACL(target_wwn=TARGET_IQN, initiator_wwn=INITIATOR_IQN)
    acl.create_acl()
    acl.set_auth(userid=USERNAME, password=PASSWORD)
    set_initiatorname(INITIATOR_IQN)

    # Test login with CHAP
    iscsiadm = IscsiAdm()
    assert iscsiadm.discovery(portal='127.0.0.1:3260').succeeded
    assert iscsiadm.discovery(portal='127.0.0.1:3260', username=USERNAME, password=PASSWORD).succeeded

    # Test mutual CHAP
    acl.set_auth(
        userid=USERNAME,
        password=PASSWORD,
        mutual_userid=MUTUAL_USERNAME,
        mutual_password=MUTUAL_PASSWORD,
    )
    assert iscsiadm.discovery(
        portal='127.0.0.1:3260',
        username=USERNAME,
        password=PASSWORD,
        mutual_username=MUTUAL_USERNAME,
        mutual_password=MUTUAL_PASSWORD,
    ).succeeded

    # Clean up
    acl.delete_acl()
    target.delete_target()
