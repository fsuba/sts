"""Test suite for sts-libs.

This package contains unit tests for:
- Core functionality
- Storage libraries
- Utility modules
- Test fixtures

Test Organization:
- Unit tests for each module
- Integration tests for key features
- Fixture tests for reusable components
"""
