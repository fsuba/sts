"""FIO test execution package.

This package provides functionality for running FIO tests:
- Parameter management
- FIO file support
- Test execution
"""
#  Copyright: Contributors to the sts project
#  GNU General Public License v3.0+ (see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import annotations
