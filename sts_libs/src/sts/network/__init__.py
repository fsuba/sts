"""Network management package.

This package provides functionality for managing network interfaces and connections:
- Interface discovery and management
- MAC address handling
- IP address management
- NetworkManager operations
"""
#  Copyright: Contributors to the sts project
#  GNU General Public License v3.0+ (see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import annotations
