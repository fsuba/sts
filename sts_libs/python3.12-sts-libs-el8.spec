%global __python3 /usr/bin/python3.12
%global python3_pkgversion 3.12

Name:           python%{python3_pkgversion}-sts-libs
Version:        0.0.1
Release:        1
Summary:        Library for pytest-based Linux storage tests

License:        GPL-3.0-or-later
URL:            https://pypi.org/project/sts-libs/
Source:         sts_libs-0.0.1.tar.gz

BuildArch:      noarch
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-rpm-macros
BuildRequires:  python%{python3_pkgversion}-flit-core
BuildRequires:  pyproject-rpm-macros


%global _description %{expand:
Library for storage testing with pytest.
Used by sts tests: https://gitlab.com/rh-kernel-stqe/sts
}

%description %_description

%prep
%autosetup -p1 -n sts_libs-0.0.1
mv sts_libs/el8_pyproject.toml pyproject.toml
mv sts_libs/src/sts ./

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files sts

%check
# TODO

%files -n python%{python3_pkgversion}-sts-libs -f %{pyproject_files}

%changelog
* Mon Aug 26 2024 Martin Hoyer <mhoyer@redhat.com> - 0.8.1-1
- Refactoring how el8 rpm is being build

* Wed Jun 26 2024 Martin Hoyer <mhoyer@redhat.com> - 0.3.1-1
- RHEL-8 base package for copr
